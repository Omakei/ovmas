@extends('layouts.garage')

@section('garage-content')

<h3>Technicians</h3><br>

<!-- Button trigger modal-->
<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">Add New Technician</button>
<!-- Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal Title</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span class="font-weight-light" aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
            <form action="{{ route('garage.technicianprofile.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">First Name</label>
                    <input class="form-control" id="name" name="first_name" type="text" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="name">Last Name</label>
                    <input class="form-control" id="name"  name="last_name" type="text" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="name">National ID</label>
                    <input class="form-control" id="name"  name="national_id" type="text" placeholder="National Id">
                </div>
                <div class="form-group">
                    <label for="name">Email</label>
                    <input class="form-control" id="name"  name="email" type="text" placeholder="email">
                </div>
                <div class="form-group">
                    <label for="name">Mobile Number</label>
                    <input class="form-control" id="name"  name="mobile_number" type="text" placeholder="Mobile Number">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Image</label>
                    <input class="form-control-file" id="exampleFormControlFile1" name="image" type="file">
                </div>
                <div class="form-group">
                    <label for="Region" >Region</label>
                    <select class="from-grop" name="region" id="Region">
                        <option selected disabled>-Select Region-</option>
                        <option value="arusha">Arusha</option>
                        <option value="dar.es.salaam">Dar es Salaam</option>
                        <option value="dodoma">Dodoma</option>
                        <option value="geita">Geita</option>
                        <option value="iringa">Iringa</option>
                        <option value="kagera">Kagera</option>
                        <option value="katavi">Katavi</option>
                        <option value="kigoma">Kigoma</option>
                        <option value="kilimanjaro">Kilimanjaro</option>
                        <option value="lindi">Lindi</option>
                        <option value="manyara">Manyara</option>
                        <option value="mara">Mara</option>
                        <option value="mbeya">Mbeya</option>
                        <option value="morogoro">Morogoro</option>
                        <option value="mtwara">Mtwara</option>
                        <option value="mwanza">Mwanza</option>
                        <option value="njombe">Njombe</option>
                        <option value="pwani">Pwani</option>
                        <option value="rukwa">Rukwa</option>
                        <option value="ruvuma">Ruvuma</option>
                        <option value="shinyanga">Shinyanga</option>
                        <option value="simiyu">Simiyu</option>
                        <option value="singida">Singida</option>
                        <option value="songwe">Songwe</option>
                        <option value="tabora">Tabora</option>
                        <option value="tanga">Tanga</option>
                    </select>
                </div>
                <div class="from-grop">
                    <label for="District">District</label>
                    <select class="from-grop" name="district" id="District" >
                        <option value="">-Select District-</option>
                        <option value="arusha" title="Arusha">Arusha</option>
                        <option value="karatu" title="Karatu">Karatu</option>
                        <option value="longido" title="Longido">Longido</option>
                        <option value="meru" title="Meru">Meru</option>
                        <option value="monduli" title="Monduli">Monduli</option>
                        <option value="ngorongoro" title="Ngorongoro">Ngorongoro</option>
                        <option value="ubungo" title="Ubungo">Ubungo</option>
                        <option value="kigamboni" title="Kigamboni">Kigamboni</option>
                        <option value="ilala" title="Ilala">Ilala</option>
                        <option value="dar.es.salam" title="Dar es Salam">Dar es Salam</option>
                        <option value="temeke" title="Temeke">Temeke</option>
                        <option value="ubungo" title="Ubungo">Ubungo</option>
                        <option value="bahi" title="Bahi">Bahi</option>
                        <option value="chamwino" title="Chamwino">Chamwino</option>
                        <option value="chemba" title="Chemba">Chemba</option>
                        <option value="dodoma" title="Dodoma">Dodoma</option>
                        <option value="kondoa" title="Kondoa">Kondoa</option>
                        <option value="kongwa" title="Kongwa">Kongwa</option>
                        <option value="mpwapwa" title="Mpwapwa">Mpwapwa</option>
                        <option value="bukombe" title="Bukombe">Bukombe</option>
                        <option value="chato" title="Chato">Chato</option>
                        <option value="geita" title="Geita">Geita</option>
                        <option value="mbogwe" title="Mbogwe">Mbogwe</option>
                        <option value="nyanghwale" title="Nyang'hwale">Nyang'hwale</option>
                        <option value="iringa" title="Iringa">Iringa</option>
                        <option value="kilolo" title="Kilolo">Kilolo</option>
                        <option value="mafinga" title="Mafinga">Mafinga</option>
                        <option value="mufindi" title="Mufindi">Mufindi</option>
                        <option value="biharamulo" title="Biharamulo">Biharamulo</option>
                        <option value="bukoba" title="Bukoba">Bukoba</option>
                        <option value="karagwe" title="Karagwe">Karagwe</option>
                        <option value="kyerwa" title="Kyerwa">Kyerwa</option>
                        <option value="missenyi" title="Missenyi">Missenyi</option>
                        <option value="muleba" title="Muleba">Muleba</option>
                        <option value="ngara" title="Ngara">Ngara</option>
                    </select>
                </div>
                <div class="from-grop">
                    <label for="Ward">Ward</label>
                    <select class="from-grop" name="ward" id="Ward" >
                        <option value="">-Select Ward-</option>
                        <option value="arusha" title="Arusha">Arusha</option>
                        <option value="karatu" title="Karatu">Karatu</option>
                        <option value="longido" title="Longido">Longido</option>
                        <option value="meru" title="Meru">Meru</option>
                        <option value="monduli" title="Monduli">Monduli</option>
                        <option value="ngorongoro" title="Ngorongoro">Ngorongoro</option>
                        <option value="ubungo" title="Ubungo">Ubungo</option>
                        <option value="kigamboni" title="Kigamboni">Kigamboni</option>
                        <option value="ilala" title="Ilala">Ilala</option>
                        <option value="dar.es.salam" title="Dar es Salam">Dar es Salam</option>
                        <option value="temeke" title="Temeke">Temeke</option>
                        <option value="ubungo" title="Ubungo">Ubungo</option>
                        <option value="bahi" title="Bahi">Bahi</option>
                        <option value="chamwino" title="Chamwino">Chamwino</option>
                        <option value="chemba" title="Chemba">Chemba</option>
                        <option value="dodoma" title="Dodoma">Dodoma</option>
                        <option value="kondoa" title="Kondoa">Kondoa</option>
                        <option value="kongwa" title="Kongwa">Kongwa</option>
                        <option value="mpwapwa" title="Mpwapwa">Mpwapwa</option>
                        <option value="bukombe" title="Bukombe">Bukombe</option>
                        <option value="chato" title="Chato">Chato</option>
                        <option value="geita" title="Geita">Geita</option>
                        <option value="mbogwe" title="Mbogwe">Mbogwe</option>
                        <option value="nyanghwale" title="Nyang'hwale">Nyang'hwale</option>
                        <option value="iringa" title="Iringa">Iringa</option>
                        <option value="kilolo" title="Kilolo">Kilolo</option>
                        <option value="mafinga" title="Mafinga">Mafinga</option>
                        <option value="mufindi" title="Mufindi">Mufindi</option>
                        <option value="biharamulo" title="Biharamulo">Biharamulo</option>
                        <option value="bukoba" title="Bukoba">Bukoba</option>
                        <option value="karagwe" title="Karagwe">Karagwe</option>
                        <option value="kyerwa" title="Kyerwa">Kyerwa</option>
                        <option value="missenyi" title="Missenyi">Missenyi</option>
                        <option value="muleba" title="Muleba">Muleba</option>
                        <option value="ngara" title="Ngara">Ngara</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Specialist At</label>
                    <textarea class="form-control" name="specialist_at" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                @if(!empty(Auth::user()->garage(Auth::user()->id)->id))
                <input class="form-control" id="name" name="garage_profile_id" value="{{ Auth::user()->garage(Auth::user()->id)->id }}" type="hidden" >
                @endif
                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
            </form>
      </div>
      <div class="modal-footer"><button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Close</button></div>
    </div>
  </div>
</div>
<br><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Full Name</th>
        <th scope="col">Mobile Number</th>
        <th scope="col">Specialist At</th>
        <th scope="col">Date</th>
      </tr>
    </thead>
    <tbody>
        @if (!empty($technicians))
            @foreach ($technicians as $technician)
            <tr>
                <th scope="row">{{ $technician->first_name }} {{ $technician->last_name }}</th>
                <td>{{ $technician->mobile_number}}</td>
                <td>{{ $technician->specialist_at}}</td>
                <td class="white-space-nowrap">{{ $technician->created_at->diffForHumans() }}</td>
            </tr>
            @endforeach
        @else
        <h3>No Technician</h3>
        @endif
    </tbody>
</table>
@endsection