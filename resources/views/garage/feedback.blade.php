@extends('layouts.garage')

@section('garage-content')

<h3>Feedbacks</h3><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Client full Name</th>
        <th scope="col">Ratings</th>
        <th scope="col">Testimonial</th>
        <th scope="col">Date</th>
      </tr>
    </thead>
    <tbody>
        @if (!empty($feedbacks))
            @foreach ($feedbacks as $feedback)
            <tr>
                <th scope="row">{{ $feedback->sender($feedback->client_profile_id)->first_name }} {{ $feedback->sender($feedback->client_profile_id)->last_name }}</th>
                <td>{{ $feedback->rateings}}</td>
                <td>{{ $feedback->testimonial}}</td>
                <td class="white-space-nowrap">{{ $feedback->created_at->diffForHumans() }}</td>
            </tr>
            @endforeach
        @else
         <h3>No Assistance Requests</h3> 
        @endif
    </tbody>
</table>
@endsection