@extends('layouts.garage')

@section('garage-content')

<h3>Assistance Request</h3><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">sender full Name</th>
        <th scope="col">Phone Number</th>
        <th scope="col">Breakdown</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @if (!$requests->isEmpty())
            @foreach ($requests as $request)
            <tr>
                <th scope="row">{{ $request->client($request->sender_id)->first_name}} {{ $request->client($request->sender_id)->last_name}}</th>
                <td>{{ $request->client($request->sender_id)->mobile_number}}</td>
                <td>{{ $request->breakdown_info}}</td>
                <td>{{ $request->status($request->id)->status }}</td>
                <td class="white-space-nowrap">
                    @if ($request->status($request->id)->status === 'Pending' )
                    <a class="btn btn-outline-success mr-1 mb-1" href="{{ url('/garage/assistancerequest/accept/'. $request->id) }}" >Accept</a> 
                    <a class="btn btn-outline-danger mr-1 mb-1" href="{{ url('/garage/assistancerequest/'. $request->id) }}" >Denied</a></td>
                    @else
                    No Action
                    @endif
            </tr>
            @endforeach
        @else
        <h3>No Assistance Requests</h3>
        @endif
    </tbody>
</table>
@endsection