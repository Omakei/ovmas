@extends('layouts.garage')
@section('garage-content')

@if (!empty($profile))
<div class="card mb-3">
    <div class="card-header position-relative min-vh-25 mb-7">
      <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image:url({{ asset('/assets/img/generic/4.jpg') }});"></div>
      <!--/.bg-holder-->
      <div class="avatar avatar-5xl avatar-profile"><img class="rounded-circle img-thumbnail shadow-sm" src="{{ asset($profile->image) }}" width="200" alt="" /></div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-8">
          <h4 class="mb-1">{{ $profile->first_name }} {{ $profile->last_name }}<small class="fas fa-check-circle text-primary ml-1" data-toggle="tooltip" data-placement="right" title="Verified" data-fa-transform="shrink-4 down-2"></small></h4>
          <h5 class="fs-0 font-weight-normal">{{ $profile->tin_number }}</h5>
          <p class="text-500">{{ $profile->region }}, {{ $profile->district }} {{ $profile->ward }}</p><button class="btn btn-falcon-primary btn-sm px-3" type="button">{{ $profile->email_for_notification }}</button><button class="btn btn-falcon-default btn-sm px-3 ml-2" type="button">{{ $profile->office_number }}</button>
          <hr class="border-dashed my-4 d-lg-none" />
        </div>
      </div>
    </div>
  <div class="row no-gutters">
    <div class="col-lg-8 pr-lg-2">
      <div class="card mb-3">
        <div class="card-header bg-light">
          <h5 class="mb-0">Service Offered</h5>
        </div>
        <div class="card-body text-justify">
          <p class="text-1000">{{ $profile->service_offered }}</p>
        </div>
      </div>
    </div>
  </div>
</div>  
@else
<div class="card mb-3">
    <div class="bg-holder d-none d-lg-block bg-card" style="background-image:url({{ asset('/assets/img/illustrations/corner-4.png') }});"></div>
    <!--/.bg-holder-->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-8">
          <h3 class="mb-0">Go to  settings in order to set up your profile</h3>
          <p class="mt-2">You must set up your  profile first so as you can be listed to customers and view other links. <br>by OVMAS admin</p>
        </div>
      </div>
    </div>
  </div>
@endif

@endsection