@extends('layouts.client')

@section('client-content')

<h3>Assistance Request</h3><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Receiver Name</th>
        <th scope="col">Phone Number</th>
        <th scope="col">Breakdown</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        
        @if (!$requests->isEmpty())
            @foreach ($requests as $request)
            <tr>
                <th scope="row">{{ $request->garage($request->receiver_id)->first_name}} {{ $request->garage($request->receiver_id)->last_name}}</th>
                <td>{{ $request->garage($request->receiver_id)->office_number}}</td>
                <td>{{ $request->breakdown_info}}</td>
                <td>{{ $request->status($request->id)->status}}</td>
                <td class="white-space-nowrap">
                    @if ($request->status($request->id)->status === 'Pending' )
                    <a class="btn btn-outline-danger mr-1 mb-1" href="{{ url('/client/assistancerequests/'. $request->id ) }}">Cancel</a>
                    @elseif ($request->status($request->id)->status === 'Accepted')
                    <button class="btn btn-outline-success mr-1 mb-1" type="button" data-toggle="modal" data-target="#exampleModal{{ $request->receiver_id}}" >Rate</button>
                    @else
                    No Action
                    @endif
                </td>
                 
            </tr>
            <!-- Modal-->
            <div class="modal fade" id="exampleModal{{ $request->receiver_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">feedback</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span class="font-weight-light" aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('client.feedback.store') }}" method="POST">
                        @csrf
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Select Rating</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="rateings">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <input class="form-control" id="name" hidden name="garage_profile_id" value="{{ $request->receiver_id}}">
                    <input class="form-control" id="name" hidden name="assistance_request_id" value="{{ $request->id}}">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Say Something About them</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="testimonial"></textarea>
                    </div>
                </div>
                <div class="modal-footer"><button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary btn-sm" type="submit">Save changes</button></div>
                </div>
            </form>
            </div>
            </div>
            @endforeach
        @else
        <h3>No Assistance Requests</h3>
        @endif
    </tbody>
</table>
@endsection