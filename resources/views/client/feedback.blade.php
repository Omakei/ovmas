@extends('layouts.client')

@section('client-content')

<h3>Feedbacks</h3><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Garage Name</th>
        <th scope="col">Ratings</th>
        <th scope="col">Testimonial</th>
        <th scope="col">Date</th>
      </tr>
    </thead>
    <tbody>
        @if (!$feedbacks->isEmpty())
            @foreach ($feedbacks as $feedback)
            <tr>
                <th scope="row">{{ $feedback->receiver($feedback->garage_profile_id)->first_name }} {{ $feedback->receiver($feedback->garage_profile_id)->last_name }}</th>
                <td>{{ $feedback->rateings}}</td>
                <td>{{ $feedback->testimonial}}</td>
                <td class="white-space-nowrap">{{ $feedback->created_at->diffForHumans() }}</td>
            </tr>
            @endforeach
        @else
        <h3>No Feedback</h3>
        @endif
    </tbody>
</table>
@endsection