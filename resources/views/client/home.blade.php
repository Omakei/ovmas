@extends('layouts.client')

@section('client-content')

<!-- Locations map start 
<div class="map" id="locations-map">
</div> -->

<h3> All Garage Avilable </h3> <br>

<div class="card" id="users" >
  <div class="card-header bg-light">
    <div class="row align-items-center">
      <div class="col">
        <h5 class="mb-0" id="followers">Garage <span class="d-none d-sm-inline-block">({{ $garages->count() }})</span></h5>
      </div><br>
      <div class="col">
        <form>
          <div class="row no-gutters">
            <div class="col"><input class="form-control form-control-sm search" type="text" placeholder="Search..." /></div>
        </form>
      </div>
    </div><br><br>
  </div>
  <div class="card-body bg-light p-0" >
    <div class="row no-gutters text-center fs--1">
        @if (!$garages->isEmpty())
        @foreach ($garages as $garage)
          <div class="col-6 col-md-4 col-lg-3 col-xxl-2 mb-1">
              <div class="bg-white p-3 h-100"><a href="#"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="{{ asset($garage->image) }}" alt="" width="100" /></a>
                  <h6 class="mb-1"><a href="garageprofile/{{ $garage->id }}" class="name">{{ $garage->first_name }} {{ $garage->last_name }}</a></h6>
                  <p class="fs--2 mb-1"><a class="text-700" href="#!" class="region">{{ $garage->region }} {{ $garage->district }} , {{ $garage->ward }}</a></p>
              </div>
          </div>  
        @endforeach
        
        @else
            <h1> No Garage Avilable </h1>
        @endif
    </div>
  </div>
</div></div>
@endsection