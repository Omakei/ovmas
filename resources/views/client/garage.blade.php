@extends('layouts.client')
@section('client-content')

@if (!empty($profile))
<div class="card mb-3">
    <div class="card-header position-relative min-vh-25 mb-7">
      <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image:url({{ asset('/assets/img/generic/4.jpg') }});"></div>
      <!--/.bg-holder-->
      <div class="avatar avatar-5xl avatar-profile"><img class="rounded-circle img-thumbnail shadow-sm" src="{{ asset($profile->image) }}" width="200" alt="" /></div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-8">
          <h4 class="mb-1">{{ $profile->first_name }} {{ $profile->last_name }}<small class="fas fa-check-circle text-primary ml-1" data-toggle="tooltip" data-placement="right" title="Verified" data-fa-transform="shrink-4 down-2"></small></h4>
          <h5 class="fs-0 font-weight-normal">{{ $profile->tin_number }}</h5>
          <p class="text-500">{{ $profile->region }}, {{ $profile->district }} {{ $profile->ward }}</p><button class="btn btn-falcon-primary btn-sm px-3" type="button">{{ $profile->email_for_notification }}</button><button class="btn btn-falcon-default btn-sm px-3 ml-2" type="button">{{ $profile->office_number }}</button>
            <!-- Button trigger modal-->
            <button class="btn btn-primary mt-3" type="button" data-toggle="modal" data-target="#exampleModal">Request For Assistance </button>
            <!-- Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Request For Assistance</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span class="font-weight-light" aria-hidden="true">&times;</span></button>
                </div>
                <form action="{{ route('client.assistancerequest.store') }}" method="POST">
                    @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Breackdown hints so as to help Technician who come to assist you</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="breakdown_info"></textarea>
                    </div>
                    <input class="form-control" id="name" hidden name="receiver_id" value="{{ $profile->id }}">
                </div>
                <div class="modal-footer"><button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary btn-sm" type="submit">Send Request</button> 
                </div>
                </div>
            </form>
            </div>
            </div>
          <hr class="border-dashed my-4 d-lg-none" />
        </div>
      </div>
    </div>
  <div class="row no-gutters">
    <div class="col-lg-8 pr-lg-2">
      <div class="card mb-3">
        <div class="card-header bg-light">
          <h5 class="mb-0">Service Offered</h5>
        </div>
        <div class="card-body text-justify">
          <p class="text-1000">{{ $profile->service_offered }}</p>
        </div>
      </div>
    </div>
  </div>
</div> 
<div class="card-body bg-light p-0">
        <h3 class="mb-0 mt-3 ml-3"> Technicians Form this Garage </h3>
        <div class="row no-gutters text-center fs--1">
            @if (!$technicians->isEmpty())
            @foreach ($technicians as $technician)
              <div class="col-6 col-md-4 col-lg-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="#"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="{{ asset($technician->image) }}" alt="" width="100" /></a>
                      <h6 class="mb-1"><a href="#">{{ $technician->first_name }} {{ $technician->last_name }}</a></h6>
                      <p class="fs--2 mb-1"><a class="text-700" href="#!">{{ $technician->specialist_at }} </a></p>
                  </div>
              </div>  
            @endforeach
           
            @else
                <h1> No Technician Avilable </h1>
            @endif
        </div>
      </div> 
@else
<div class="card mb-3">
    <div class="bg-holder d-none d-lg-block bg-card" style="background-image:url({{ asset('/assets/img/illustrations/corner-4.png') }});"></div>
    <!--/.bg-holder-->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-8">
          <h3 class="mb-0">The Garage that you want to Request for a Service has not yet set the Profile</h3>
          <p class="mt-2">wait untill they finish to fill their profile. Sorry for disturbance you can still request service form other garages. <br>by OVMAS admin</p>
        </div>
      </div>
    </div>
  </div>
@endif

@endsection