@extends('layouts.client')
@section('client-content')
@if (!empty($profile))
<form action="{{ route('client.clientprofile.update') }}" method="POST">
    @csrf
    @method('PATCH')
    <div class="row">
        <div class="col-12">
          <div class="card mb-3 btn-reveal-trigger">
            <div class="card-header position-relative min-vh-25 mb-8">
              <div class="cover-image">
                <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image:url({{ asset('/assets/img/generic/4.jpg') }});"></div>
                <!--/.bg-holder-->
                <input class="d-none" id="upload-cover-image" type="file"><label class="cover-image-file-input" for="upload-cover-image"><span class="fas fa-camera mr-2"></span><span>Change cover photo</span></label>
              </div>
              <div class="avatar avatar-5xl avatar-profile shadow-sm img-thumbnail rounded-circle">
                <div class="h-100 w-100 rounded-circle overflow-hidden position-relative"> <img src="{{ asset($profile->image) }}" width="200" alt=""><input class="d-none" id="profile-image" type="file"><label class="mb-0 overlay-icon d-flex flex-center" for="profile-image"><span class="bg-holder overlay overlay-0"></span><span class="z-index-1 text-white text-center fs--1"><span class="fas fa-camera"></span><span class="d-block">Update</span></span></label></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-8 pr-lg-2">
          <div class="card mb-3">
            <div class="card-header">
              <h5 class="mb-0">Profile Settings</h5>
            </div>
            <div class="card-body bg-light">
              
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group"><label for="first-name">First Name</label><input class="form-control" id="first-name" name="first_name" type="text" value="{{ $profile->first_name }}"></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group"><label for="last-name">Last Name</label><input class="form-control" id="last-name" name="last_name" type="text" value="{{ $profile->last_name }}"></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group"><label for="email1">Email </label><input class="form-control" id="email1" name="email_for_notification" type="text" value="{{ Auth::user()->email }}" disabled></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group"><label for="phone">Mobile Phone</label><input class="form-control" id="phone" name="mobile_number" type="text" value="{{ $profile->mobile_number }}"></div>
                  </div>
                  <div class="col-12">
                    <div class="form-group"><label for="heading">National Id</label><input class="form-control" id="heading" name="national_id" type="text" value="{{ $profile->national_id }}"></div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                        <label for="Region" >Region</label>
                        <select name="region" id="Region">
                            <option value="{{ $profile->region }}" selected >{{ $profile->region }}</option>
                            <option value="arusha">Arusha</option>
                            <option value="dar.es.salaam">Dar es Salaam</option>
                            <option value="dodoma">Dodoma</option>
                            <option value="geita">Geita</option>
                            <option value="iringa">Iringa</option>
                            <option value="kagera">Kagera</option>
                            <option value="katavi">Katavi</option>
                            <option value="kigoma">Kigoma</option>
                            <option value="kilimanjaro">Kilimanjaro</option>
                            <option value="lindi">Lindi</option>
                            <option value="manyara">Manyara</option>
                            <option value="mara">Mara</option>
                            <option value="mbeya">Mbeya</option>
                            <option value="morogoro">Morogoro</option>
                            <option value="mtwara">Mtwara</option>
                            <option value="mwanza">Mwanza</option>
                            <option value="njombe">Njombe</option>
                            <option value="pwani">Pwani</option>
                            <option value="rukwa">Rukwa</option>
                            <option value="ruvuma">Ruvuma</option>
                            <option value="shinyanga">Shinyanga</option>
                            <option value="simiyu">Simiyu</option>
                            <option value="singida">Singida</option>
                            <option value="songwe">Songwe</option>
                            <option value="tabora">Tabora</option>
                            <option value="tanga">Tanga</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-12">
                      <div class="from-grop">
                        <label for="District">District</label>
                        <select name="district" id="District" >
                            <option value="{{ $profile->district }}">{{ $profile->district }}</option>
                            <option value="arusha" title="Arusha">Arusha</option>
                            <option value="karatu" title="Karatu">Karatu</option>
                            <option value="longido" title="Longido">Longido</option>
                            <option value="meru" title="Meru">Meru</option>
                            <option value="monduli" title="Monduli">Monduli</option>
                            <option value="ngorongoro" title="Ngorongoro">Ngorongoro</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="kigamboni" title="Kigamboni">Kigamboni</option>
                            <option value="ilala" title="Ilala">Ilala</option>
                            <option value="dar.es.salam" title="Dar es Salam">Dar es Salam</option>
                            <option value="temeke" title="Temeke">Temeke</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="bahi" title="Bahi">Bahi</option>
                            <option value="chamwino" title="Chamwino">Chamwino</option>
                            <option value="chemba" title="Chemba">Chemba</option>
                            <option value="dodoma" title="Dodoma">Dodoma</option>
                            <option value="kondoa" title="Kondoa">Kondoa</option>
                            <option value="kongwa" title="Kongwa">Kongwa</option>
                            <option value="mpwapwa" title="Mpwapwa">Mpwapwa</option>
                            <option value="bukombe" title="Bukombe">Bukombe</option>
                            <option value="chato" title="Chato">Chato</option>
                            <option value="geita" title="Geita">Geita</option>
                            <option value="mbogwe" title="Mbogwe">Mbogwe</option>
                            <option value="nyanghwale" title="Nyang'hwale">Nyang'hwale</option>
                            <option value="iringa" title="Iringa">Iringa</option>
                            <option value="kilolo" title="Kilolo">Kilolo</option>
                            <option value="mafinga" title="Mafinga">Mafinga</option>
                            <option value="mufindi" title="Mufindi">Mufindi</option>
                            <option value="biharamulo" title="Biharamulo">Biharamulo</option>
                            <option value="bukoba" title="Bukoba">Bukoba</option>
                            <option value="karagwe" title="Karagwe">Karagwe</option>
                            <option value="kyerwa" title="Kyerwa">Kyerwa</option>
                            <option value="missenyi" title="Missenyi">Missenyi</option>
                            <option value="muleba" title="Muleba">Muleba</option>
                            <option value="ngara" title="Ngara">Ngara</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-12">
                    <div class="from-grop">
                        <label for="Ward">Ward</label>
                        <select name="ward" id="Ward" >
                            <option value="{{ $profile->ward }}">{{ $profile->ward }}</option>
                            <option value="arusha" title="Arusha">Arusha</option>
                            <option value="karatu" title="Karatu">Karatu</option>
                            <option value="longido" title="Longido">Longido</option>
                            <option value="meru" title="Meru">Meru</option>
                            <option value="monduli" title="Monduli">Monduli</option>
                            <option value="ngorongoro" title="Ngorongoro">Ngorongoro</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="kigamboni" title="Kigamboni">Kigamboni</option>
                            <option value="ilala" title="Ilala">Ilala</option>
                            <option value="dar.es.salam" title="Dar es Salam">Dar es Salam</option>
                            <option value="temeke" title="Temeke">Temeke</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="bahi" title="Bahi">Bahi</option>
                            <option value="chamwino" title="Chamwino">Chamwino</option>
                            <option value="chemba" title="Chemba">Chemba</option>
                            <option value="dodoma" title="Dodoma">Dodoma</option>
                            <option value="kondoa" title="Kondoa">Kondoa</option>
                            <option value="kongwa" title="Kongwa">Kongwa</option>
                            <option value="mpwapwa" title="Mpwapwa">Mpwapwa</option>
                            <option value="bukombe" title="Bukombe">Bukombe</option>
                            <option value="chato" title="Chato">Chato</option>
                            <option value="geita" title="Geita">Geita</option>
                            <option value="mbogwe" title="Mbogwe">Mbogwe</option>
                            <option value="nyanghwale" title="Nyang'hwale">Nyang'hwale</option>
                            <option value="iringa" title="Iringa">Iringa</option>
                            <option value="kilolo" title="Kilolo">Kilolo</option>
                            <option value="mafinga" title="Mafinga">Mafinga</option>
                            <option value="mufindi" title="Mufindi">Mufindi</option>
                            <option value="biharamulo" title="Biharamulo">Biharamulo</option>
                            <option value="bukoba" title="Bukoba">Bukoba</option>
                            <option value="karagwe" title="Karagwe">Karagwe</option>
                            <option value="kyerwa" title="Kyerwa">Kyerwa</option>
                            <option value="missenyi" title="Missenyi">Missenyi</option>
                            <option value="muleba" title="Muleba">Muleba</option>
                            <option value="ngara" title="Ngara">Ngara</option>
                        </select>
                      </div>
                  </div>
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                  <div class="col-12 d-flex justify-content-end"><button class="btn btn-primary" type="submit">Update </button></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>  
@else
<form action="{{ route('client.clientprofile.store') }}" method="POST">
  @csrf
    <div class="row">
        <div class="col-12">
          <div class="card mb-3 btn-reveal-trigger">
            <div class="card-header position-relative min-vh-25 mb-8">
              <div class="cover-image">
                <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image:url({{ asset('/assets/img/generic/4.jpg') }});"></div>
                <!--/.bg-holder-->
                <input class="d-none" id="upload-cover-image" type="file"><label class="cover-image-file-input" for="upload-cover-image"><span class="fas fa-camera mr-2"></span><span>Change cover photo</span></label>
              </div>
              <div class="avatar avatar-5xl avatar-profile shadow-sm img-thumbnail rounded-circle">
                <div class="h-100 w-100 rounded-circle overflow-hidden position-relative"> <img src="{{ asset('/assets/img/team/2.jpg') }}" width="200" alt=""><input class="d-none" id="profile-image" name="image" type="file"><label class="mb-0 overlay-icon d-flex flex-center" for="profile-image"><span class="bg-holder overlay overlay-0"></span><span class="z-index-1 text-white text-center fs--1"><span class="fas fa-camera"></span><span class="d-block">Update</span></span></label></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-8 pr-lg-2">
          <div class="card mb-3">
            <div class="card-header">
              <h5 class="mb-0">Profile Settings</h5>
            </div>
            <div class="card-body bg-light">
              
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group"><label for="first-name">First Name</label><input class="form-control" id="first-name" name="first_name" type="text" ></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group"><label for="last-name">Last Name</label><input class="form-control" id="last-name" name="last_name" type="text" ></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group"><label for="phone">Office Phone</label><input class="form-control" id="phone" name="mobile_number" type="text" ></div>
                  </div>
                  <div class="col-12">
                    <div class="form-group"><label for="heading">National Id</label><input class="form-control" id="heading" name="national_id" type="text" ></div>
                  </div>
                 
                  <div class="col-12">
                    <div class="form-group">
                        <label for="Region" >Region</label>
                        <select name="region" id="Region">
                            <option selected disabled>-Select Region-</option>
                            <option value="arusha">Arusha</option>
                            <option value="dar.es.salaam">Dar es Salaam</option>
                            <option value="dodoma">Dodoma</option>
                            <option value="geita">Geita</option>
                            <option value="iringa">Iringa</option>
                            <option value="kagera">Kagera</option>
                            <option value="katavi">Katavi</option>
                            <option value="kigoma">Kigoma</option>
                            <option value="kilimanjaro">Kilimanjaro</option>
                            <option value="lindi">Lindi</option>
                            <option value="manyara">Manyara</option>
                            <option value="mara">Mara</option>
                            <option value="mbeya">Mbeya</option>
                            <option value="morogoro">Morogoro</option>
                            <option value="mtwara">Mtwara</option>
                            <option value="mwanza">Mwanza</option>
                            <option value="njombe">Njombe</option>
                            <option value="pwani">Pwani</option>
                            <option value="rukwa">Rukwa</option>
                            <option value="ruvuma">Ruvuma</option>
                            <option value="shinyanga">Shinyanga</option>
                            <option value="simiyu">Simiyu</option>
                            <option value="singida">Singida</option>
                            <option value="songwe">Songwe</option>
                            <option value="tabora">Tabora</option>
                            <option value="tanga">Tanga</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-12">
                      <div class="from-grop">
                        <label for="District">District</label>
                        <select name="district" id="District" >
                            <option value="">-Select District-</option>
                            <option value="arusha" title="Arusha">Arusha</option>
                            <option value="karatu" title="Karatu">Karatu</option>
                            <option value="longido" title="Longido">Longido</option>
                            <option value="meru" title="Meru">Meru</option>
                            <option value="monduli" title="Monduli">Monduli</option>
                            <option value="ngorongoro" title="Ngorongoro">Ngorongoro</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="kigamboni" title="Kigamboni">Kigamboni</option>
                            <option value="ilala" title="Ilala">Ilala</option>
                            <option value="dar.es.salam" title="Dar es Salam">Dar es Salam</option>
                            <option value="temeke" title="Temeke">Temeke</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="bahi" title="Bahi">Bahi</option>
                            <option value="chamwino" title="Chamwino">Chamwino</option>
                            <option value="chemba" title="Chemba">Chemba</option>
                            <option value="dodoma" title="Dodoma">Dodoma</option>
                            <option value="kondoa" title="Kondoa">Kondoa</option>
                            <option value="kongwa" title="Kongwa">Kongwa</option>
                            <option value="mpwapwa" title="Mpwapwa">Mpwapwa</option>
                            <option value="bukombe" title="Bukombe">Bukombe</option>
                            <option value="chato" title="Chato">Chato</option>
                            <option value="geita" title="Geita">Geita</option>
                            <option value="mbogwe" title="Mbogwe">Mbogwe</option>
                            <option value="nyanghwale" title="Nyang'hwale">Nyang'hwale</option>
                            <option value="iringa" title="Iringa">Iringa</option>
                            <option value="kilolo" title="Kilolo">Kilolo</option>
                            <option value="mafinga" title="Mafinga">Mafinga</option>
                            <option value="mufindi" title="Mufindi">Mufindi</option>
                            <option value="biharamulo" title="Biharamulo">Biharamulo</option>
                            <option value="bukoba" title="Bukoba">Bukoba</option>
                            <option value="karagwe" title="Karagwe">Karagwe</option>
                            <option value="kyerwa" title="Kyerwa">Kyerwa</option>
                            <option value="missenyi" title="Missenyi">Missenyi</option>
                            <option value="muleba" title="Muleba">Muleba</option>
                            <option value="ngara" title="Ngara">Ngara</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-12">
                    <div class="from-grop">
                        <label for="Ward">Ward</label>
                        <select name="ward" id="Ward" >
                            <option value="">-Select Ward-</option>
                            <option value="arusha" title="Arusha">Arusha</option>
                            <option value="karatu" title="Karatu">Karatu</option>
                            <option value="longido" title="Longido">Longido</option>
                            <option value="meru" title="Meru">Meru</option>
                            <option value="monduli" title="Monduli">Monduli</option>
                            <option value="ngorongoro" title="Ngorongoro">Ngorongoro</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="kigamboni" title="Kigamboni">Kigamboni</option>
                            <option value="ilala" title="Ilala">Ilala</option>
                            <option value="dar.es.salam" title="Dar es Salam">Dar es Salam</option>
                            <option value="temeke" title="Temeke">Temeke</option>
                            <option value="ubungo" title="Ubungo">Ubungo</option>
                            <option value="bahi" title="Bahi">Bahi</option>
                            <option value="chamwino" title="Chamwino">Chamwino</option>
                            <option value="chemba" title="Chemba">Chemba</option>
                            <option value="dodoma" title="Dodoma">Dodoma</option>
                            <option value="kondoa" title="Kondoa">Kondoa</option>
                            <option value="kongwa" title="Kongwa">Kongwa</option>
                            <option value="mpwapwa" title="Mpwapwa">Mpwapwa</option>
                            <option value="bukombe" title="Bukombe">Bukombe</option>
                            <option value="chato" title="Chato">Chato</option>
                            <option value="geita" title="Geita">Geita</option>
                            <option value="mbogwe" title="Mbogwe">Mbogwe</option>
                            <option value="nyanghwale" title="Nyang'hwale">Nyang'hwale</option>
                            <option value="iringa" title="Iringa">Iringa</option>
                            <option value="kilolo" title="Kilolo">Kilolo</option>
                            <option value="mafinga" title="Mafinga">Mafinga</option>
                            <option value="mufindi" title="Mufindi">Mufindi</option>
                            <option value="biharamulo" title="Biharamulo">Biharamulo</option>
                            <option value="bukoba" title="Bukoba">Bukoba</option>
                            <option value="karagwe" title="Karagwe">Karagwe</option>
                            <option value="kyerwa" title="Kyerwa">Kyerwa</option>
                            <option value="missenyi" title="Missenyi">Missenyi</option>
                            <option value="muleba" title="Muleba">Muleba</option>
                            <option value="ngara" title="Ngara">Ngara</option>
                        </select>
                    </div>
                  </div>
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                  <div class="col-12 d-flex justify-content-end"><button class="btn btn-primary" type="submit">Update </button></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endif

@endsection