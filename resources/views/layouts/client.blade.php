<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OVMAS') }}</title>

    <!-- Scripts -->
   
    <script src="{{ asset('jss/app.js') }}" defer></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/assets/img/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/assets/img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/img/favicons/favicon-16x16.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/assets/img/favicons/favicon.ico') }}">
    <link rel="manifest" href="{{ asset('/assets/img/favicons/manifest.json') }}">
    <meta name="msapplication-TileImage" content="{{ asset('/assets/img/favicons/mstile-150x150.png') }}">
    {{--  <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="{{ asset('/assets/lib/owl.carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/lib/fancybox/jquery.fancybox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/lib/flatpickr/flatpickr.min.css') }}" rel="stylesheet">  --}}
    <link href="{{ asset('/assets/css/theme.css') }}" rel="stylesheet">
    {{--  <link href="{{ asset('css/app.css') }}" rel="stylesheet">  --}}
</head>
<body>
        <script>
                var options = {
                    valueNames: [ 'name', 'region' ]
                  };
                  
                  var userList = new List('users', options);
        </script>
    <main class="main" id="top">
        <div class="container">
        <nav class="navbar navbar-vertical navbar-expand-xl navbar-light navbar-glass"><a class="navbar-brand text-left" href="{{ url('/') }}">
            <div class="d-flex align-items-center py-3"><img class="mr-2" src="{{ asset('/assets/img/illustrations/falcon.png') }}" alt="" width="40" /><span class="text-sans-serif">{{ config('app.name', 'OVMAS') }}</span></div>
            </a>
            <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
            <hr class="border-300 my-2" />
            <ul class="navbar-nav flex-column">
                <li class="nav-item"><a class="nav-link" href="{{ route('client.clientprofile.index') }}">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-home"></span></span><span>Home</span></div>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('client.assistancerequest.index1') }}">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-comments"></span></span><span>Assistance Request</span></div>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('client.feedback.index1') }}">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-thumbs-up"></span></span><span>feedback</span></div>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('client/clientprofile/' . Auth::user()->id) }}">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-user"></span></span><span>Profile</span></div>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('client/clientprofile/'. Auth::user()->id . '/edit') }}">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-cog"></span></span><span>Settings</span></div>
                </a></li>
            </ul>
            </div>
        </nav>
        <div class="content">
            <nav class="navbar navbar-light navbar-glass fs--1 font-weight-semi-bold row navbar-top sticky-kit navbar-expand"><button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button><a class="navbar-brand text-left ml-3" href="../index.html">
                <div class="d-flex align-items-center"><img class="mr-2" src="{{ asset('/assets/img/illustrations/falcon.png') }}" alt="" width="40" /><span class="text-sans-serif">OVMAS</span></div>
            </a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown1">
                <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item dropdown"><a class="nav-link unread-indicator px-0" id="navbarDropdownNotification" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-bell fs-4" data-fa-transform="shrink-6"></span></a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-card" aria-labelledby="navbarDropdownNotification">
                    <div class="card card-notification shadow-none" style="max-width: 20rem">
                        <div class="card-header">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                            <h6 class="card-header-title mb-0">Notifications</h6>
                            </div>
                            <div class="col-auto"><a class="card-link font-weight-normal" href="#">Mark all as read</a></div>
                        </div>
                        </div>
                        <div class="list-group list-group-flush font-weight-normal fs--1">
                        <div class="list-group-title">NEW</div>
                        @foreach (Auth::user()->unreadNotifications as $notification )
                        <div class="list-group-item">
                        <a class="notification notification-flush bg-200" href="#!">
                                <div class="notification-avatar">
                                    <div class="avatar avatar-2xl mr-3">
                                        <div class="avatar-name rounded-circle"><span>OM</span></div>
                                    </div>
                                </div>
                                <div class="notification-body">
                                    <p class="mb-1"><strong>{{ $notification->data['client'] }} OVMAS User</strong> {{ $notification->data['BreakdownHints'] }}</p>
                                    
                                    <span class="notification-time"><span class="mr-1" role="img" aria-label="Emoji"></span>{{ \Carbon\Carbon::parse($notification->data['date'])->diffForHumans() }}</span>
                                </div>
                                </a>
                            </div> 
                        @endforeach
                        </div> 
                        <div class="card-footer text-center border-top-0"><a class="card-link d-block" href="notifications.html">View all</a></div>
                    </div>
                    </div>
                </li>
                <li class="nav-item dropdown"><a class="nav-link pr-0" id="navbarDropdownUser" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   @if (!empty(Auth::user()->client(Auth::user()->id)->image ))
                   <div class="avatar avatar-xl">
                        <img class="rounded-circle" src="{{ asset(Auth::user()->client(Auth::user()->id)->image )  }}" alt="" />
                    </div>
                   @else
                   <div class="avatar avatar-xl">
                        <img class="rounded-circle" src="{{  asset('/assets/img/illustrations/falcon.png') }}" alt="" />
                    </div>
                   @endif
                   
                    </a>
                    <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="navbarDropdownUser">
                    <div class="bg-white rounded-soft py-2">
                        <a class="dropdown-item font-weight-bold text-warning" href="#!"><span class="fas fa-crown mr-1"></span><span>OVMAS</span></a>
                        <div class="dropdown-divider"></div>
                        @if (!empty(Auth::user()->client(Auth::user()->id)->first_name))
                        <a class="dropdown-item" href="#">{{ Auth::user()->client(Auth::user()->id)->first_name }} {{ Auth::user()->client(Auth::user()->id)->last_name }}</a>
                        @else
                        <a class="dropdown-item" href="#">{{ Auth::user()->email }}</a>
                        @endif
                        
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                        </form>
                    </div>
                    </div>
                </li>
                </ul>
            </div>
            </nav>
            @yield('client-content')
            <footer>
            <div class="row no-gutters justify-content-between fs--1 mt-4 mb-3">
                <div class="col-12 col-sm-auto text-center">
                <p class="mb-0 text-600">We provide good service <span class="d-none d-sm-inline-block">| </span><br class="d-sm-none" /> 2019 &copy; <a href="#">OVMAS</a></p>
                </div>
                <div class="col-12 col-sm-auto text-center">
                <p class="mb-0 text-600">Dj Omakei</p>
                </div>
            </div>
            </footer>
        </div>
        </div>
    </main>

   
    {{--  <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>  --}}
    {{--  <script src="{{ asset('/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.js') }}"></script>
    <script src="{{ asset('/assets/js/plugins.js') }}"></script>  --}}
    {{--  <script src="{{ asset('/assets/lib/stickyfilljs/stickyfill.min.js') }}"></script>
    <script src="{{ asset('/assets/lib/sticky-kit/sticky-kit.min.js') }}"></script>
    <script src="{{ asset('/assets/lib/owl.carousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('/assets/lib/fancybox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('/assets/lib/flatpickr/flatpickr.min.js') }}"></script>  --}}
    {{--  <script src="{{ asset('/assets/js/theme.js') }}"></script>  --}}
    <script src="http://www.coffeecreamthemes.com/themes/taxigrabber/html/js/uber-google-maps.min.js"></script> 
</body>
</html>
