@extends('layouts.admin')

@section('admin-content')

<h3>Users</h3><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">full Name</th>
        <th scope="col">Email</th>
        <th scope="col">Mobile Number</th>
        <th scope="col">Feedbacks (Ratings)</th>
        <td scope="col"> Status</td>
        <th scope="col">Register Date</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @if (!$users->isEmpty())
            @foreach ($users as $user)
            <tr>
                <th>
                    @if ($user->role === 'Garage')
                    {{ $user->garage($user->id)['first_name'] }} {{ $user->garage($user->id)['last_name'] }}   
                    @else
                    {{ $user->client($user->id)['first_name']  }} {{ $user->client($user->id)['last_name'] }}
                    @endif
                    </th>
                <td>
                    @if ($user->role === 'Garage')
                    {{ $user->garage($user->id)['email_for_notification'] }}   
                    @else
                    {{ $user->email }}
                    @endif
                </td>
                <td>
                    @if ($user->role === 'Garage')
                    {{ $user->garage($user->id)['office_number'] }}  
                    @else
                    {{ $user->client($user->id)['mobile_number'] }}
                    @endif
                </td>
                <td>
                    @if ($user->role === 'Garage')
                    @foreach ($user->feedbacks($user->garage($user->id)['id']) as $feedback )
                    {{ $feedback->rateings }} , 
                    @endforeach
                    @else
                    --
                    @endif
                </td>
                <td>{{ $user->status }}</td>
                <td class="white-space-nowrap">{{ $user->created_at->diffForHumans() }}</td>
                <td>
                    <a class="btn btn-outline-success mr-1 mb-1" href="{{ url('/admin/users/activate/' . $user->id) }}" >Active</a> 
                    <a class="btn btn-outline-danger mr-1 mb-1" href="{{ url('/admin/users/deactivate/'. $user->id) }}" >Deactivate</a></td>
                </td>
            </tr>
            @endforeach
        @else
        <h3>No Users </h3>
        @endif
    </tbody>
</table>
@endsection