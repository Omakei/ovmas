<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'garage',
    'middleware' => 'garage',
], function () {
    Route::post('garageprofile', 'GarageProfileController@store')->name('garage.garageprofile.store');
    Route::get('garageprofile', 'GarageProfileController@index')->name('garage.garageprofile.index');
    Route::get('garageprofile/create', 'GarageProfileController@create')->name('garage.garageprofile.create');
    Route::get('garageprofile/{id}/edit', 'GarageProfileController@edit')->name('garage.garageprofile.edit');
    Route::get('garageprofile/{id}', 'GarageProfileController@show')->name('garage.garageprofile.show');
    Route::patch('garageprofile/', 'GarageProfileController@update')->name('garage.garageprofile.update');

    Route::post('feedback', 'FeedbackController@store')->name('garage.feedback.store');
    Route::get('feedback', 'FeedbackController@index')->name('garage.feedback.index');
    Route::get('feedback/create', 'FeedbackController@create')->name('garage.feedback.create');
    Route::get('feedback/{id}/edit', 'FeedbackController@edit')->name('garage.feedback.edit');
    Route::patch('feedback/{id}', 'FeedbackController@update')->name('garage.feedback.update');

    Route::post('technicianprofile', 'TechnicianProfileController@store')->name('garage.technicianprofile.store');
    Route::get('technicianprofile', 'TechnicianProfileController@index')->name('garage.technicianprofile.index');
    Route::get('technicianprofile/create', 'TechnicianProfileController@create')->name('garage.technicianprofile.create');
    Route::get('technicianprofile/{id}/edit', 'TechnicianProfileController@edit')->name('garage.technicianprofile.edit');
    Route::patch('technicianprofile/{id}', 'TechnicianProfileController@update')->name('garage.technicianprofile.update');

    Route::post('assistancerequest', 'AssistanceRequestController@store')->name('garage.assistancerequest.store');
    Route::get('assistancerequest', 'AssistanceRequestController@index')->name('garage.assistancerequest.index');
    Route::get('assistancerequest/accept/{id}', 'AssistanceRequestController@accept')->name('garage.assistancerequest.accept');
    Route::get('assistancerequest/{id}', 'AssistanceRequestController@denied')->name('garage.assistancerequest.denied');
    Route::patch('assistancerequest/{id}', 'AssistanceRequestController@update')->name('garage.assistancerequest.update');
});

Route::group([
    'prefix' => 'client',
    'middleware' => 'client',
], function () {

    Route::post('feedback', 'FeedbackController@store')->name('client.feedback.store');
    Route::get('feedback', 'FeedbackController@index')->name('client.feedback.index');
    Route::get('feedback/view', 'FeedbackController@index1')->name('client.feedback.index1');
    Route::get('feedback/{id}/edit', 'FeedbackController@edit')->name('client.feedback.edit');
    Route::patch('feedback/{id}', 'FeedbackController@update')->name('client.feedback.update');

    Route::post('assistancerequest', 'AssistanceRequestController@store')->name('client.assistancerequest.store');
    Route::get('assistancerequest', 'AssistanceRequestController@index')->name('client.assistancerequest.index');
    Route::get('assistancerequest/view', 'AssistanceRequestController@index1')->name('client.assistancerequest.index1');
    Route::get('assistancerequest/{id}/edit', 'AssistanceRequestController@edit')->name('client.assistancerequest.edit');
    Route::patch('assistancerequest/{id}', 'AssistanceRequestController@update')->name('client.assistancerequest.update');
    Route::get('assistancerequests/{id}', 'AssistanceRequestController@destroy')->name('client.assistancerequest.delete');

    Route::post('clientprofile', 'ClientProfileController@store')->name('client.clientprofile.store');
    Route::get('clientprofile', 'ClientProfileController@index')->name('client.clientprofile.index');
    Route::get('clientprofile/create', 'ClientProfileController@create')->name('client.clientprofile.create');
    Route::get('clientprofile/{id}/edit', 'ClientProfileController@edit')->name('client.clientprofile.edit');
    Route::get('clientprofile/{id}', 'ClientProfileController@show')->name('client.clientprofile.show');
    Route::patch('clientprofile', 'ClientProfileController@update')->name('client.clientprofile.update');
    Route::get('garageprofile/{id}', 'ClientProfileController@garage')->name('client.clientprofile.garage');
});

Route::group([
    'prefix' => 'admin',
    'middleware' => 'admin', 
], function () {
    Route::post('users', 'UserManagerController@store')->name('admin.store');
    Route::get('users', 'UserManagerController@index')->name('admin.index');
    Route::get('users/activate/{id}', 'UserManagerController@activate')->name('admin.acti');
    Route::get('users/deactivate/{id}', 'UserManagerController@deactivate')->name('admin.edit');
    Route::patch('users/{id}', 'UserManagerController@update')->name('admin.update');
});
