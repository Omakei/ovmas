<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarageProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tin_number');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image');
            $table->string('region');
            $table->string('district');
            $table->string('ward');
            $table->string('email_for_notification');
            $table->string('office_number');
            $table->longText('service_offered');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garage_profiles');
    }
}
