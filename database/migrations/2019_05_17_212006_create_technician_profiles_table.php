<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicianProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technician_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('national_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('image');
            $table->string('region');
            $table->string('district');
            $table->string('ward');
            $table->string('email');
            $table->string('mobile_number');
            $table->longText('specialist_at');
            $table->unsignedBigInteger('garage_profile_id');
            $table->foreign('garage_profile_id')->references('id')->on('garage_profiles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technician_profiles');
    }
}
