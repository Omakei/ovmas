<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistanceRequest extends Model
{
    protected $fillable = [
        'breakdown_info', 'receiver_id', 'sender_id', 
    ];

    public function sender($id)
    {
        return User::where('id', $id)->first();
    }
    
    public function receiver($id)
    {
        return User::where('id', $id)->first();
    }
    
    public function garage($id)
    {
        return GarageProfile::where('user_id', $id)->first();
    }

    public function client($id)
    {
        return ClientProfile::where('user_id', $id)->first();
    }

    public function statuses()
    {
        return $this->hasMany('App\Status');
    }

    public function status($id)
    {
        return Status::where('assistance_request_id', $id)->first();
    }
}
