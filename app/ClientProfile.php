<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientProfile extends Model
{
    protected $fillable = [
        'national_id', 'first_name', 'last_name', 'image', 'region', 'district', 'ward', 'mobile_number', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function assistance_requests()
    {
        return $this->hasMany('App\AssistanceRequest');
    }
}
