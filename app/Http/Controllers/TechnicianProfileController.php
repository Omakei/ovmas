<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\AssistanceRequest;
use App\Feedback;
use App\GarageProfile;
use App\TechnicianProfile;
use App\ClientProfile;
use App\User;
use Validator;

class TechnicianProfileController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty(Auth::user()->garage(Auth::user()->id)->id)) {
            $technicians = TechnicianProfile::where('garage_profile_id', Auth::user()->garage(Auth::user()->id)->id)->get();

            return //$technicians;
            view('garage.technician', ['technicians' => $technicians]);
        }else {
            $technicians = null;
            return //$technicians;
            view('garage.technician', ['technicians' => $technicians]);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'national_id' => 'required|string',
            'last_name' => 'required|string',
            'mobile_number' => 'required|string',
            'email' => 'required|string',
            'specialist_at' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'ward' => 'required|string',
            'garage_profile_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            if($request->hasFile('image')){
                $filename_ext = $request->file('image')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('image')->storeAs('public/tech/image', $filetostore);
                $profile = TechnicianProfile::create([
                'first_name' => $request['first_name'],
                'national_id' => $request['national_id'],
                'last_name' => $request['last_name'],
                'mobile_number' => $request['mobile_number'],
                'email' => $request['email'],
                'specialist_at' => $request['specialist_at'],
                'region' => $request['region'],
                'district' => $request['district'],
                'ward' => $request['ward'],
                'garage_profile_id' => $request['garage_profile_id'],
                'image' => $path,
                
                ]);

                return redirect()->back();  

            }else{
                    $profile = TechnicianProfile::create([
                        'first_name' => $request['first_name'],
                        'national_id' => $request['national_id'],
                        'last_name' => $request['last_name'],
                        'mobile_number' => $request['mobile_number'],
                        'email' => $request['email'],
                        'specialist_at' => $request['specialist_at'],
                        'region' => $request['region'],
                        'district' => $request['district'],
                        'ward' => $request['ward'],
                        'garage_profile_id' => $request['garage_profile_id'],
                        'image' => '',
                    ]); 
            }
            return redirect()->back();
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
