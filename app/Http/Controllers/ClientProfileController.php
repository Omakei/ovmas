<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\AssistanceRequest;
use App\Feedback;
use App\GarageProfile;
use App\TechnicianProfile;
use App\ClientProfile;
use App\User;
use Validator;

class ClientProfileController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $garages = GarageProfile::all();
       // return $garages;
        return view('client.home', ['garages' => $garages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function garage($id)
    {
        $profile = GarageProfile::where('id', $id)->first();
       // dd($profile);
        if(!empty($profile)){
            $technicians = TechnicianProfile::where('garage_profile_id', $profile->id)->get();
            return //$profile;
            view('client.garage', ['profile' => $profile, 'technicians' =>$technicians]);
        } else {
            $profile = GarageProfile::where('id', $id)->first();
            $technicians = null;
            return //$profile;
            view('client.garage', ['profile' => $profile, 'technicians' =>$technicians]);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'national_id' => 'required|string',
            'last_name' => 'required|string',
            'mobile_number' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'ward' => 'required|string',
            'user_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            if($request->hasFile('image')){
                $filename_ext = $request->file('image')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('image')->storeAs('public/tech/image', $filetostore);
                $profile = ClientProfile::create([
                'first_name' => $request['first_name'],
                'national_id' => $request['national_id'],
                'last_name' => $request['last_name'],
                'mobile_number' => $request['mobile_number'],
                'region' => $request['region'],
                'district' => $request['district'],
                'ward' => $request['ward'],
                'image' => $path,
                'user_id' => $request['user_id']
                
                ]);
                       // dd($profile);
                return redirect()->back();  

            }else{
                $profile = ClientProfile::create([
                    'first_name' => $request['first_name'],
                    'national_id' => $request['national_id'],
                    'last_name' => $request['last_name'],
                    'mobile_number' => $request['mobile_number'],
                    'region' => $request['region'],
                    'district' => $request['district'],
                    'ward' => $request['ward'],
                    'user_id' => $request['user_id'],
                    'image' => '/assets/img/illustrations/falcon.png'
                    
                ]);
               // dd($profile);
            }
            return redirect()->back();
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $profile = ClientProfile::where('user_id', $id)->first();
        
        return //$profile;
        view('client.profile', ['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $profile = ClientProfile::where('user_id', $id)->first();

        return //$profile;
        view('client.setting', ['profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'national_id' => 'required|string',
            'last_name' => 'required|string',
            'mobile_number' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'ward' => 'required|string',
            'user_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            if($request->hasFile('image')){
                $filename_ext = $request->file('image')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('image')->storeAs('public/tech/image', $filetostore);
                $profile = ClientProfile::where('user_id', $request['user_id'])->update([
                'first_name' => $request['first_name'],
                'national_id' => $request['national_id'],
                'last_name' => $request['last_name'],
                'mobile_number' => $request['mobile_number'],
                'region' => $request['region'],
                'district' => $request['district'],
                'ward' => $request['ward'],
                'image' => $path,
                
                ]);
                       // dd($profile);
                return redirect()->back();  

            }else{
                $profile = ClientProfile::where('user_id', $request['user_id'])->update([
                    'first_name' => $request['first_name'],
                    'national_id' => $request['national_id'],
                    'last_name' => $request['last_name'],
                    'mobile_number' => $request['mobile_number'],
                    'region' => $request['region'],
                    'district' => $request['district'],
                    'ward' => $request['ward'],
                    
                ]);
               // dd($profile);
            }
            return redirect()->back();
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
