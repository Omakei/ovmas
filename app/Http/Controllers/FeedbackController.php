<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Notifications\RateRequest;
use App\AssistanceRequest;
use App\Feedback;
use App\GarageProfile;
use App\TechnicianProfile;
use App\ClientProfile;
use App\User;
use App\Status;
use Validator;

class FeedbackController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty(Auth::user()->garage(Auth::user()->id)->id)) {
            $feedbacks = Feedback::where('garage_profile_id', Auth::user()->garage(Auth::user()->id)->id)->with('client', 'garage')->get();

            return //$feedbacks;
            view('garage.feedback', ['feedbacks' => $feedbacks]);
        } else {
            $feedbacks = null;

            return //$feedbacks;
            view('garage.feedback', ['feedbacks' => $feedbacks]);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index1()
    {
        $feedbacks = Feedback::where('client_profile_id', Auth::user()->client(Auth::user()->id)->id)->with('client', 'garage')->get();

        return //$feedbacks;
        view('client.feedback', ['feedbacks' => $feedbacks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'rateings' => 'required|string',
            'testimonial' => 'required|string',
            'garage_profile_id' => 'required|string',
            'assistance_request_id' => 'required|string'
            
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            $user = GarageProfile::where('user_id', $request['garage_profile_id'])->first();
            //dd($user);
            $profile = Feedback::create([
                'rateings' => $request['rateings'],
                'testimonial' => $request['testimonial'],
                'client_profile_id' => Auth::user()->client(Auth::user()->id)->id,
                'garage_profile_id' => $user->id     
            ]);
            $status = Status::where('assistance_request_id', $request['assistance_request_id'])->first();
            $status->status = 'Rated';
            $status->save();

            $request = AssistanceRequest::where('id', $request['assistance_request_id'])->first();
            $user = User::where('id', $request->receiver_id)->first();
    
            $user->notify(New RateRequest($request));
        
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
