<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Notifications\NewRequest;
use App\Notifications\CanceledRequest;
use App\Notifications\AcceptedRequest;
use App\Notifications\DeclinedRequest;
use Illuminate\Http\Request;
use App\AssistanceRequest;
use App\Feedback;
use App\GarageProfile;
use App\TechnicianProfile;
use App\ClientProfile;
use App\User;
use App\Status;
use Validator;

class AssistanceRequestController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = AssistanceRequest::where('receiver_id', Auth::user()->id)->get();
        //return \response()->json($requests);
        return view('garage.request', ['requests' => $requests]);
        
        //$requests->client($requests->sender_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index1()
    {
        $requests = AssistanceRequest::where('sender_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        
        return 
        view('client.request', ['requests' => $requests]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'breakdown_info' => 'required|string',
            'receiver_id' => 'required|string',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            $user = GarageProfile::where('id', $request['receiver_id'])->first();
            $profile = AssistanceRequest::create([
                'breakdown_info' => $request['breakdown_info'],
                'receiver_id' => $user->user_id,
                'sender_id' => Auth::user()->id,    
            ]);
            $status = new Status;
            $status->status = 'Pending';
            $status->assistance_request_id = $profile->id;
            $status->save();

            $userforNotify = User::where('id', $user->user_id)->first();

            $userforNotify->notify(new NewRequest($profile));
        
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accept($id)
    {
        $status = Status::where('assistance_request_id',  $id)->first();
        $status->status = 'Accepted';
        $status->save();

        $request = AssistanceRequest::where('id', $id)->first();
        $user = User::where('id', $request->sender_id)->first();
        $user->notify(new AcceptedRequest($request));

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function denied($id)
    {
        $status = Status::where('assistance_request_id',  $id)->first();
        $status->status = 'Denied';
        $status->save();

        $request = AssistanceRequest::where('id', $id)->first();
        $user = User::where('id', $request->sender_id)->first();
        $user->notify(new DeclinedRequest($request));

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status =Status::where('assistance_request_id',  $id)->first();
        $status->status = 'Canceled';
        $status->save();

        $request = AssistanceRequest::where('id', $id)->first();
        $user = User::where('id', $request->receiver_id)->first();

        $user->notify(New CanceledRequest($request));

        return redirect()->back();
    }
}
