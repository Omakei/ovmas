<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\AssistanceRequest;
use App\Feedback;
use App\GarageProfile;
use App\TechnicianProfile;
use App\ClientProfile;
use App\User;
use Validator;

class GarageProfileController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('garage.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'tin_number' => 'required|string',
            'last_name' => 'required|string',
            'office_number' => 'required|string',
            'email_for_notification' => 'required|string',
            'service_offered' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'ward' => 'required|string',
            'user_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            if($request->hasFile('image')){
                $filename_ext = $request->file('image')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('image')->storeAs('public/tech/image', $filetostore);
                $profile = GarageProfile::create([
                'first_name' => $request['first_name'],
                'tin_number' => $request['tin_number'],
                'last_name' => $request['last_name'],
                'office_number' => $request['office_number'],
                'email_for_notification' => $request['email_for_notification'],
                'service_offered' => $request['service_offered'],
                'region' => $request['region'],
                'district' => $request['district'],
                'ward' => $request['ward'],
                'user_id' => $request['user_id'],
                'image' => $path,
                
                ]);

                return redirect()->back();  

            }else{
                $profile = GarageProfile::create([
                    'first_name' => $request['first_name'],
                    'tin_number' => $request['tin_number'],
                    'last_name' => $request['last_name'],
                    'office_number' => $request['office_number'],
                    'email_for_notification' => $request['email_for_notification'],
                    'service_offered' => $request['service_offered'],
                    'region' => $request['region'],
                    'district' => $request['district'],
                    'ward' => $request['ward'],
                    'user_id' => $request['user_id'],
                    'image' => '/assets/img/illustrations/falcon.png',
                    
                ]);
    
            }
            return redirect()->back();
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = GarageProfile::where('user_id', $id)->first();
        
        return //$profile;
        view('garage.profile', ['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = GarageProfile::where('user_id', $id)->first();

        return //$profile;
        view('garage.settings', ['profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'tin_number' => 'required|string',
            'last_name' => 'required|string',
            'office_number' => 'required|string',
            'email_for_notification' => 'required|string',
            'service_offered' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'ward' => 'required|string',
            'user_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            if($request->hasFile('image')){
                $filename_ext = $request->file('image')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('image')->storeAs('public/tech/image', $filetostore);
                $profile = GarageProfile::where('user_id', $request['user_id'])->update([
                'first_name' => $request['first_name'],
                'tin_number' => $request['tin_number'],
                'last_name' => $request['last_name'],
                'office_number' => $request['office_number'],
                'email_for_notification' => $request['email_for_notification'],
                'service_offered' => $request['service_offered'],
                'region' => $request['region'],
                'district' => $request['district'],
                'ward' => $request['ward'],
                'image' => $path,
                
                ]);
                       // dd($profile);
                return redirect()->back();  

            }else{
                $profile = GarageProfile::where('user_id', $request['user_id'])->update([
                    'first_name' => $request['first_name'],
                    'tin_number' => $request['tin_number'],
                    'last_name' => $request['last_name'],
                    'office_number' => $request['office_number'],
                    'email_for_notification' => $request['email_for_notification'],
                    'service_offered' => $request['service_offered'], 
                    'region' => $request['region'],
                    'district' => $request['district'],
                    'ward' => $request['ward'],
                    
                ]);
               // dd($profile);
            }
            return redirect()->back();
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
