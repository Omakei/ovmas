<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'rateings', 'testimonial', 'client_profile_id', 'garage_profile_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\ClientProfile');
    }

    public function garage()
    {
        return $this->belongsTo('App\GarageProfile');
    }

    public function sender($id)
    {
        return ClientProfile::where('id', $id)->first();
    }

    public function receiver($id)
    {
        return GarageProfile::where('id', $id)->first();
    }
}
