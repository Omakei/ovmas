<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CanceledRequest extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($assistanceRequest)
    {
        $this->assistanceRequest = $assistanceRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                        ->subject('Canceled Request')
                        ->line('The user in OVMAS system thats request Breakdown Service from you canceled the request.')
                        ->line('Visit the system for more information.')
                        ->action('Go to System', url('/garage/assistancerequest'))
                        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'BreakdownHints' => $this->assistanceRequest->breakdown_info,
            'client' => $this->assistanceRequest->sender($this->assistanceRequest->sender_id)->first_name,
            'date' => $this->assistanceRequest->created_at
        ];
    }
}
