<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function garages()
    {
        return $this->hasMany('App\GarageProfile');
    }

    public function clients()
    {
        return $this->hasMany('App\ClientProfile');
    }

    public function hasRole($role)
    {
        $ag = Auth::user()->role;
        if ($ag === $role) {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        $admin_emails = config('ovmas.administrators');
        
        foreach($admin_emails as $admin_email) {

            if (Auth::user()->email === $admin_email) {
                return true;
            }
            return false;
        }
 
    } 

    public function garage($id)
    {
        return GarageProfile::where('user_id', $id)->first();
    }

    public function client($id)
    {
        return ClientProfile::where('user_id', $id)->first();
    }

    public function feedbacks($id){
        return Feedback::where('garage_profile_id', $id)->get();
    }
}
