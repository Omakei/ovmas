<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    

    public function assistance_request()
    {
        return $this->belongsTo('App\AssistanceRequest');
    }
}
