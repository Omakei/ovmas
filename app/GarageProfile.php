<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GarageProfile extends Model
{
    protected $fillable = [
        'tin_number', 'first_name', 'last_name', 'image', 'region', 'district', 'ward', 'email_for_notification' , 'office_number', 'service_offered', 'user_id'
    ];

    public function assistance_requests()
    {
        return $this->hasMany('AssistanceRequest');
    }

    public  function user()
    {
        return $this->belongsTo('App\User');
    }

    public function technicians()
    {
        return $this->hasMany('App\TechnicianProfile');
    }
}
