<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicianProfile extends Model
{
    protected $fillable = [
        'national_id', 'first_name', 'last_name', 'image', 'region', 'district', 'ward', 'email' , 'mobile_number', 'garage_profile_id', 'specialist_at'
    ];

    public function garage()
    {
        return $this->belongsTo('App\GarageProfile');
    }
}
